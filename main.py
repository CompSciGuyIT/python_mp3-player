# An MP3 player project
# Using Python interpreter 3.6 for use of playsound module

# Some possible functionality:
# Play MP3s
# Create playlists
# Loop songs and playlists

from glob import glob
from playsound import playsound
import os
import re
import sys

song_regex = '(?<=MP3s/).+?(?=.mp3)'


# Get list of songs from music directory
def get_directory_songlist(file):
    with open(f'{file}', 'r') as reader:
        music_path = reader.readline()
    return glob(os.path.join(music_path, '*.mp3'))


# Get list of songs from file
def get_file_songlist(file):
    with open(f'{file}', 'r') as reader:
        return reader.readlines()


# List songs
def list_songs(file):
    if file == "music_directory.txt":
        songlist = get_directory_songlist(file)
    else:
        songlist = get_file_songlist(file)
    count = 0
    print("\nSong List\n---------")
    for song in songlist:
        count += 1
        a = re.search(song_regex, song)
        print(str(count) + ": " + a.group())
    return songlist


# Add song loop
def add_song_loop(option):
    while True:
        # Display current playlist
        print("\nPlaylist", end=" ")
        list_songs('playlist.txt')

        # Display songs in music directory
        print("\nMusic Directory", end=" ")
        song_list = list_songs('music_directory.txt')

        # Add song to playlist
        choice = input(f"\nWhich song would you like to add to the playlist? [1-{len(song_list)}]"
                       f" or 'Q' to quit to menu: ")
        if choice == "Q" or choice == 'q':
            return

        # Write song to file
        index = int(choice) - 1
        with open('playlist.txt', f'{option}') as writer:
            writer.write(f"{song_list[index]}\n")


# Main Menu
class MainMenu:
    def switch(self, case):
        default = "Incorrect choice"
        return getattr(self, 'case_' + str(case), lambda: default)()

    # Play song
    @staticmethod
    def case_1():
        # Choose song
        songlist = list_songs('music_directory.txt')
        choice = input(f"\nWhich song would you like to play? [1-{len(songlist)}] or 'Q' to quit to menu: ")
        if choice == "Q" or choice == 'q':
            return
        index = int(choice) - 1

        # Play song
        print(f"Now playing: {re.search(song_regex, songlist[index]).group()}")
        playsound(songlist[index])

    # Play current playlist
    @staticmethod
    def case_2():
        songlist = list_songs('playlist.txt')
        for song in songlist:
            song = song[:-1]    # Remove new line character
            print(f"Now playing: {re.search(song_regex, song).group()}")
            playsound(song)

    # Show current playlist
    @staticmethod
    def case_3():
        list_songs('playlist.txt')

    # Create playlist
    @staticmethod
    def case_4():
        add_song_loop('w')

    # Add to current playlist
    @staticmethod
    def case_5():
        add_song_loop('a')

    # Enter directory path
    @staticmethod
    def case_6():
        with open('music_directory.txt', 'w') as writer:
            directory = input("Enter absolute path to your music directory: ")
            writer.write(directory)

    # End Program
    @staticmethod
    def case_7():
        sys.exit("Ending Program")


# Program loop
while True:
    print("\n----------\nMP3 player\n----------\n")
    print("1. Play song")
    print("2. Play current playlist\n")
    print("3. Show current playlist")
    print("4. Create new playlist")
    print("5. Add to current playlist\n")
    print("6. Enter directory path\n")
    print("7. Quit program")
    selection = int(input("\nMake selection by number: "))

    m = MainMenu()
    m.switch(selection)
